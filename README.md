# Моя конфигурация Archlinux для WSL2 (Windows 10)

Здесь представлена инструкция об установке и базовой конфигурации **Archlinux** на **Windows Subsystem for Linux 2** под **Windows 10**.

Я написал её, в первую очередь, для себя, на случай, если придётся делать это повторно, чтобы сэкономить время на гуглёж. Но я буду искренне рад, если она кому-то поможет.

## Установка

1. Первым делом нужно включить **WSL** в «Компонентах Windows»:
    
    > Панель управления → Программы и компоненты → Включение или отключение компонентов Windows → **Подсистема Windows для Linux**

    И перезагрузить систему.

2. Теперь необходимо установить версию WSL по умолчанию:
    ```PS
    PS> wsl --set-dafult-version 2
    ```

3. Скачиваем `Arch.zip` [последнего релиза ArchWSL](https://github.com/yuk7/ArchWSL/releases) и распаковываем в папку, в которой будет установлен дистрибутив.

4. Переходим в эту папку в консоли и запускаем `Arch.exe`.

5. Заходим в WSL, это можно сделать несколькими способами:
    * Ещё раз запустив `Arch.exe` в консоли.
    * `wsl -d Arch`
    * Если установлен Windows Terminal, там автоматически создастся конфигурация для дистрибутива и достаточно будет создать новую вкладку с этой конфигурацией.

5. В файле `/etc/pacman.d/mirrorlist` раскомментируем нужные зеркала, например https-зеркало Яндекса. Стандартное американское зеркало тоже оставляем раскомментированным, на всякий случай, оно не мешает.

6. Обновляем ключи pacman и всю систему:
    ```
    # pacman-key --init
    # pacman-key --populate
    # pacman-key --refresh-keys
    # pacman -Syyu
    ```

## Создание пользователя без root-прав

Создаём пользователя, задаём ему пароль:
```
# useradd -m -G users,wheel -s /bin/bash <username>
# passwd <username>
```

Если нет `sudo`, устанавливаем его:
```
# pacman -S sudo
```

В файле `/etc/sudoers` раскомментируем **одну** из этих строчек
```sh
%wheel ALL=(ALL:ALL) ALL
%wheel ALL=(ALL:ALL) NOPASSWD: ALL # если мы хотим не вводить пароль каждый раз при команде `sudo`
```

## Конфигурация WSL для дистрибутива

Редактируем файл `/etc/wsl.conf`. Например, так:
```ini
[automount]
enabled = true
options = "metadata,uid=1000,gid=1000,umask=22,fmask=11,case=off"
mountFsTab = true
crossDistro = true

[network]
generateHosts = false
generateResolvConf = true

[interop]
enabled = true
appendWindowsPath = true

[user]
default = <username>
```

Подробнее обо всех этих настройках можно почитать на [соответствующей странице MSDN](https://docs.microsoft.com/en-us/windows/wsl/wsl-config).

## Установка Arch дистрибутивом WSL по умолчанию

```PS
PS> wsl --set-default Arch
```

## Установка yay

**yay** — очень полезная утилита, позволяющая автоматизировать сборку исходных кодов из **Arch AUR**. Кроме того, `yay` функционирует, как обёртка над `pacman`, так что после его установки можно вообще все пакеты устанавливать с помощью него. Маст хэв. Далее будут приводиться команды с использованием `yay`.
```
$ sudo pacman -S git openssh
$ sudo pacman -S base-devel
```
**Внимание!** Нужно отказаться заменять `fakeroot-tcp` на `fakeroot`.
```
$ git clone https://aur.archlinux.org/yay-git.git
$ cd yay-git
$ makepkg -sric
$ yay -Su
```

Стоит отметить, что `yay` не рекомендуется запускать от рута. Он сам попросит ввести пароль (или не попросит, если в `/etc/sudoers` у нас раскомментирована строчка с `NOPASSWD`) и выполнит от рута те действия, которые требуют повышенных привилегий.

## Настройка systemd

С системами инициализации, коей является **systemd**, в WSL проблемы, но есть два способа реализовать некое подобие её правильной работы.

### Первый способ: «вручную», через утилиту daemonize

Устанавливаем `daemonize`:
```
$ yay -S daemonize
```

Создаём скрипт `/etc/profile.d/00-wsl2-systemd.sh`:
```sh
SYSTEMD_PID=$(ps -ef | grep '/lib/systemd/systemd --system-unit=basic.target$' | grep -v unshare | awk '{print $2}')

if [ -z "$SYSTEMD_PID" ]; then
sudo /usr/bin/daemonize /usr/bin/unshare --fork --pid --mount-proc /lib/systemd/systemd --system-unit=basic.target
SYSTEMD_PID=$(ps -ef | grep '/lib/systemd/systemd --system-unit=basic.target$' | grep -v unshare | awk '{print $2}')
fi

if [ -n "$SYSTEMD_PID" ] && [ "$SYSTEMD_PID" != "1" ]; then
    exec sudo /usr/bin/nsenter -t $SYSTEMD_PID -a su - $LOGNAME
fi
```
После перезапуска дистрибутива systemd должен работать.

Минусом этого способа является то, что мы не можем относительно без костылей задать фоновую инициализацию вместе с входом в Windows. Если это необходимо, см. второй способ.

### Второй способ: genie-systemd

Устанавливаем genie
```
$ yay -S genie-systemd-git
```

После установки меняем конфигурацию в файле `/etc/genie.ini` (важно сделать таймаут поменьше и `clone-path=true`). Например, у меня файл выглядит так:
```ini
[genie]
systemd-timeout=60
clone-env=WSL_DISTRO_NAME,WSL_INTEROP,WSLENV,DISPLAY,WAYLAND_DISPLAY,PULSE_SERVER
secure-path=/lib/systemd:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
clone-path=true
target-warning=true
update-hostname=true
update-hostname-suffix=-wsl
resolved-stub=false
```

Запускать дистрибутив теперь нужно вот так:
```PS
PS> wsl -d Arch genie -s
```
то есть, нужно изменить соответствующую конфигурацию в Windows Terminal, ярлыках и т.д.

Можно ещё в планировщике задач Windows создать задачу, которая при входе в систему будет запускать дистрибутив с genie командой `wsl -d Arch genie -i`. Флаг `-i`, в отличие от `-s` не запускает шелл, а только инициализирует genie.

**Важно!** С запуском некоторых сервисов systemd в genie есть некоторые проблемы, поэтому первый запуск может затянуться на весь таймаут, указанный в `/etc/genie.ini`. После него выведутся ошибки запуска всех служб. На [wiki репозитория genie](https://github.com/arkane-systems/genie/wiki) есть [описания и решения известных проблем](https://github.com/arkane-systems/genie/wiki/Systemd-units-known-to-be-problematic-under-WSL).

## Доступ к дистрибутиву по сети из Windows

Связь между Windows и нашим дистрибутивом организована через виртуальный сетевой адаптер **vEthernet (WSL)**. При этом, при каждом запуске дистрибутив получает новый IP-адрес. Чтобы каждый раз не возиться с этими постоянно меняющимися IP-адресами, можно воспользоваться утилитой [go-wsl2-host](https://github.com/shayne/go-wsl2-host). Она создаёт в Windows специальную службу, которая будет следить за запуском дистрибутивов WSL2 и задавать для каждого из них хост с соответствующим именем в файле `hosts`. В нашем случае это будет хост с именем `arch.wsl`.

## Графический интерфейс и рабочий стол

1. В первую очередь, устанавливаем любую среду рабочего стола, которая нам нравится. Я выбрал **XFCE**:
    ```
    $ yay -S xfce4
    ```
    Всё необходимое, включая `xorg`, должно подтянуться автоматически как зависимости.

2. Важно заменить `dbus` на `dbus-x11`:
    ```
    $ yay -S dbus-x11
    ```

3. Далее нам нужен `xrdp` (RDP-сервер) и бэкенд `xorgxrdp` для него. Лучше использовать версию с `glamor`, чтобы картинка была плавнее, лучше поддерживалась прозрачность и т.д.:
    ```
    $ yay -S xrdp xorgxrdp-glamor
    ```

4. Не забываем добавить загрузку модуля `glamoregl` в файле `/etc/X11/xrdp/xorg.conf`:
    ```sh
    Section "Module"
        Load "dbe"
        Load "ddc"
        Load "extmod"
        Load "glx"
        Load "int10"
        Load "record"
        Load "vbe"
        Load "glamoregl" # <---
        Load "xorgxrdp"
        Load "fb"
    EndSection
    ```

5. Копируем в домашнюю папку нашего пользователя системный файл `xinitrc` (в домашней папке он должен начинаться с точки):
    ```
    $ cp /etc/X11/xinit/xinitrc ~/.xinitrc
    ```

6. В только что скопированном файле заменяем в самом конце строчки инициализации `twm` на инициализацию XFCE:
    ```sh
    #twm &
    #xclock -geometry 50x50-1+1 &
    #xterm -geometry 80x50+494+51 &
    #xterm -geometry 80x20+494-0 &
    #exec xterm -geometry 80x66+0+0 -name login

    exec startxfce4
    ```

7. Добавляем в автозапуск и запускаем сервис `xrdp`:
    ```
    $ sudo systemctl enable xrdp
    $ sudo systemctl start xrdp
    ```

Теперь можно подключаться к рабочему столу XFCE из Windows по протоколу RDP (в Windows есть встроенный клиент «Подключение к удаленному рабочему столу», но можно воспользоваться любым сторонним) по адресу `arch.wsl` (см. раздел [Доступ к дистрибутиву по сети из Windows](#доступ-к-дистрибутиву-по-сети-из-windows)).

При входе каждый раз придётся вводить имя и пароль пользователя Linux, однако мы можем упростить себе задачу для повседневного использования, создав `.rdp`-файл с настройками где-нибудь, например, на рабочем столе. Пароль в нём должен быть в зашифрованном виде. Чтобы зашифровать его, придётся воспользоваться PowerShell:
```PS
PS> (Read-Host 'Password' -AsSecureString) | ConvertFrom-SecureString;
Password: ***
01000000d08c9ddf0115d1118c7a00c04fc297eb01000000a03db21b11e6c545a6b7338457022fe300000000020000000000106600000001000020000000fce50f66416b4bdcbfd422de69c296123efc03b731227a69facd5a6f64744482000000000e8000000002000020000000ad5f66376e2a20bc46fdb5b4b8b9ed4cb641fb1d9c8066e31f3260c27232862c1000000016b1a4a3af23a60e3f015a75d2a0387a4000000056c2c74474bd187fa54fed8fbf20a5a95c95fff28978ffc6dfdff92b06b3dadc1b55ce038efe1a9c31feb636d59209dc75e9f517df1d38b21f76e6ba6b0f733c
```

Содержимое `.rdp`-файла:
```rdp
full address:s:arch.wsl
screen mode id:i:2
use multimon:i:0
desktopwidth:i:3200
desktopheight:i:1800
session bpp:i:32
winposstr:s:0,1,203,243,2835,1708
compression:i:1
keyboardhook:i:2
audiocapturemode:i:0
videoplaybackmode:i:1
connection type:i:6
networkautodetect:i:0
bandwidthautodetect:i:1
displayconnectionbar:i:0
enableworkspacereconnect:i:0
disable wallpaper:i:0
allow font smoothing:i:1
allow desktop composition:i:1
disable full window drag:i:0
disable menu anims:i:0
disable themes:i:0
disable cursor setting:i:0
bitmapcachepersistenable:i:1
audiomode:i:0
redirectprinters:i:0
redirectcomports:i:0
redirectsmartcards:i:1
redirectclipboard:i:1
redirectposdevices:i:0
drivestoredirect:s:C:\;
autoreconnection enabled:i:1
authentication level:i:2
prompt for credentials:i:0
negotiate security layer:i:1
remoteapplicationmode:i:0
alternate shell:s:
shell working directory:s:
gatewayhostname:s:
gatewayusagemethod:i:4
gatewaycredentialssource:i:4
gatewayprofileusagemethod:i:0
promptcredentialonce:i:0
gatewaybrokeringtype:i:0
use redirection server name:i:0
rdgiskdcproxy:i:0
kdcproxyname:s:
smart sizing:i:1
username:s:<username>
password 51:b:<encrypted password>
```

Теперь можно подключаться двойным кликом по этому файлу.

## Исправление ошибки systemd-logind: failed to get session

Если в логе `~/.xorgxrdp.10.log` возникает подобная ошибка:
```
(EE) systemd-logind: failed to get session: PID ___ does not belong to any known session
```
Это происходит из-за корявой конфигурации **pam**. По той же причине не создаётся папка пользователя в `/run/user` и не работает **dbus**. Проблема присутствует с 2020 года и даже в 2022 никому не сдалось её решать, поэтому если на момент прочтения этой инструкции она ещё присутствует, решаем своими силами.

Открываем файл `/etc/pam.d/system-auth` и полностью заменяем его содержимое на следующее:
```
#%PAM-1.0

auth      required  pam_unix.so     try_first_pass nullok
auth      optional  pam_permit.so
auth      required  pam_env.so

account   required  pam_unix.so
account   optional  pam_permit.so
account   required  pam_time.so

password  required  pam_unix.so     try_first_pass nullok sha512 shadow
password  optional  pam_permit.so

session   required  pam_limits.so
session   required  pam_unix.so
session   optional  pam_permit.so
```

## Пользовательские папки

Чтобы создать в папке `$HOME` пользовательские папки, типа Documents, Downloads, Pictures и т.д. (не просто создать, а так, чтобы система их подхватила), можно воспользоваться улититой `xdg-user-dirs-update`:
```
$ yay -S xdg-user-dirs
$ xdg-user-dirs-update
```

Если мы хотим, чтобы графический файловый менеджер тоже был в курсе о них и отображал в боковой панели, нужно сделать ещё одну манипуляцию:
```
$ yay -S xdg-user-dirs-gtk
$ xdg-user-dirs-gtk-update
```

## Предложения о монтировании файловых систем в файловом менеджере

Если мы хотим, чтобы наш графический файловый менеджер предлагал нам в один клик смонтировать накопители, которые можно смонтировать, например файловые системы других WSL-дистрибутивов, подключенные флешки и т.д., нужно установить пакет `gvfs`:
```
$ yay -S gvfs
```

Кроме того, может потребоваться плагин для файлового менеджера. Например, Thunar использует `thunar-volman`, поэтому нужно убедиться, что он также установлен.

Среди предложений о монтировании может отображаться раздел на 256 GiB, на котором лежит всего один файл с именем `file`. Это файл, который WSL2 использует в качестве общей подкачки для всех дистрибутивов. Если мозолит глаза, можно создать правило для `udev`, которое его скроет.

Монтируем раздел кликом в файловом менеджере и пишем команду
```
$ mount
```

Ищем в выводе наш раздел (скорее всего, он будет выведен последним) и смотрим имя файла устройства. Велика вероятность, что это будет `/dev/sda`. Если нет, в следующих действиях необходимо подставить соответствующее имя.

Теперь создаём файл `/etc/udev/rules.d/99-hide-swap-partition.rules` со следующей строчкой:
```sh
KERNEL=="sda", ENV{UDISKS_IGNORE}="1"
```

Далее исполняем команды
```
$ sudo udevadm trigger
$ sudo umount /dev/sda
```

Если диск не пропал, перезагружаем WSL.

### USB-накопители

Если хотим монтировать флешки, надо смотреть [куда-то сюда](https://github.com/jovton/USB-Storage-on-WSL2). Сам не пробовал. Понадобится — попробую и обновлю статью.

## Звук

Для работы звука необходим `pulseaudio`:
```
$ yay -S pulseaudio
```

Можно, также, установить плагин для XFCE, чтобы добавить микшер громкости на панель:
```
$ yay -S xfce4-pulseaudio-plugin pavucontrol
```

## Оформление

Мне очень нравится тема **Kali-Dark** из Kali-linux, поэтому я нагло стырил её. Все необходимые файлы (в т.ч. цветовые схемы для терминалов `xfce4-terminal` и `qterminal`) есть в архиве в репозитории.

```
$ git clone https://gitlab.com/lem0nify/arch-wsl2-config.git
$ sudo tar -xvzkf ./arch-wsl2-config/kali-dark-theme.tar.gz -C /usr/share
```

Кроме того, я предпочитаю использовать **Whisker** в качестве альтернативы стандартному меню XFCE:
```
$ yay -S xfce4-whiskermenu-plugin
```

Индикатор раскладки клавиатуры для панели XFCE тоже устанавливается отдельно:
```
$ yay -S xfce4-xkb-plugin
```

## Автодополнение по TAB в терминале
No comments. Не представляю, как без этого вообще можно жить.
```
$ yay -S bash-completion
```

## Заголовок и цвета в терминале

Редактируем `/etc/bash.bashrc`:
```sh
#
# /etc/bash.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ $DISPLAY ]] && shopt -s checkwinsize

# Change the window title of X terminals
case ${TERM} in
        [aEkx]term*|rxvt*|gnome*|konsole*|interix|tmux*)
                PS1='\[\033]0;\u@\h:\w\007\]'
                ;;
        screen*)
                PS1='\[\033k\u@\h:\w\033\\\]'
                ;;
        *)
                unset PS1
                ;;
esac

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.
# We run dircolors directly due to its changes in file syntax and
# terminal name patching.
use_color=false
if type -P dircolors >/dev/null ; then
        # Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
        LS_COLORS=
        if [[ -f ~/.dir_colors ]] ; then
                eval "$(dircolors -b ~/.dir_colors)"
        elif [[ -f /etc/DIR_COLORS ]] ; then
                eval "$(dircolors -b /etc/DIR_COLORS)"
        else
                eval "$(dircolors -b)"
        fi
        # Note: We always evaluate the LS_COLORS setting even when it's the
        # default.  If it isn't set, then `ls` will only colorize by default
        # based on file attributes and ignore extensions (even the compiled
        # in defaults of dircolors). #583814
        if [[ -n ${LS_COLORS:+set} ]] ; then
                use_color=true
        else
                # Delete it if it's empty as it's useless in that case.
                unset LS_COLORS
        fi
else
        # Some systems (e.g. BSD & embedded) don't typically come with
        # dircolors so we need to hardcode some terminals in here.
        case ${TERM} in
        [aEkx]term*|rxvt*|gnome*|konsole*|screen|tmux|cons25|*color) use_color=true;;
        esac
fi

if ${use_color} ; then
        if [[ ${EUID} == 0 ]] ; then
                PS1+='\[\033[01;91m\]\u@\h \[\033[01;94m\]\w \[\033[00m\]\$ '
        else
                PS1+='\[\033[01;94m\]\u@\h \[\033[01;92m\]\w \[\033[00m\]\$ '
        fi

        alias diff='diff --color=auto'
        alias grep='grep --colour=auto'
        alias egrep='grep -E --colour=auto'
        alias fgrep='grep -F --colour=auto'
        alias ip='ip --color=auto'
        alias l='ls -CF'
        alias la='ls -A'
        alias ll='ls -l'
        alias ls='ls --color=auto'
        export MANPAGER="less -R --use-color -Dd+b -Du+g"
else
        # show root@ when we don't have colors
        PS1+='\u@\h \w \$ '
fi

unset use_color

# Bash completion
[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion
```

Вот и всё. У нас есть пригодный для использования и даже почти полноценный **Archlinux** на **WSL2**.
